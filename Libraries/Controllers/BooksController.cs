﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Libraries.Models;

namespace Libraries.Controllers
{
    public class BooksController : Controller
    {
        private LibraryDbContext db = new LibraryDbContext();

        // GET: Books
        public ActionResult Index(string term )
        {
            var books = from m in db.Books
                         select m;
            /*
            if (string.IsNullOrWhiteSpace(term))
            {
                books = db.Books.ToList();
            }
            else
            {
                books = db.Books.ToList().Where(book => book.Title.Contains(term)).ToList();
            }
            */
            if (!String.IsNullOrEmpty(term))
            {
                books = books.Where(s => s.Title.Contains(term));
            }
        
            return View(books);
        }

        // GET: Books/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // GET: Books/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Books/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Author")] Book book)
        {
            if (ModelState.IsValid)
            {
                db.Books.Add(book);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(book);
        }

        [HttpPost]
        public JsonResult CreateWithAjax(Book book)
        {
            if (ModelState.IsValid)
            {
                book.Id = db.Books.Count() + 1;
                db.Books.Add(book);
                db.SaveChanges();
                return Json(new Response {
                       Success=true,
                       Data=book
                });
            }

            return Json(new Response());
        }
        [HttpPost]
        public JsonResult EditWithAjax(Book book)
        {
            
           

            if (ModelState.IsValid)
            {
                Book currentBook = db.Books.Find(book.Id);
                db.Entry(currentBook).State = EntityState.Modified;
                if (currentBook != null)
                {
                    currentBook.Title = book.Title;
                    currentBook.Author = book.Author;
                    db.SaveChanges();
                    return Json(new Response
                    {
                        Success = true,
                        Data = book
                    });
                }
            }
            return Json(new Response());
        }

        [HttpGet]
        public PartialViewResult ListPartial(string term)
        {

            List<Book> books = null;
            if (string.IsNullOrWhiteSpace(term))
            {
                books = db.Books.ToList();
            }
            else
            {
                books = db.Books.ToList().Where(book => book.Title.Contains(term)).ToList();
            }

            return PartialView("_Table",books);
        }

        [HttpGet]
        public PartialViewResult SaveBook(int? id, int? op)
        {
            Book book = null;
            if (id == null)
            {
                book = new Book() { Id = -1 };
                
            }
            else
            {
                book = db.Books.FirstOrDefault(bok => bok.Id == id.Value);
                if (op == -2)
                {
                    ViewBag.op = -2;
        
                }
               
            }

            return PartialView("_SaveContent", book);
        }


        // GET: Books/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return PartialView("_Edit",book);
        }

        // POST: Books/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Author")] Book book)
        {
            if (ModelState.IsValid)
            {
                db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(book);
        }

        // GET: Books/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Book book = db.Books.Find(id);
            db.Books.Remove(book);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
