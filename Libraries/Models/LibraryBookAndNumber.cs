﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Libraries.Models
{
    public class LibraryBookAndNumber
    {
        public virtual int Id { get; set; }
        public virtual int BookId { get; set; }
        public virtual int Number { get; set; }
        public virtual int LibraryId { get; set; }

        public virtual Library Library { get; set; }
        public virtual Book Book { get; set; }
    }
}