﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Libraries.Models
{
    public class Library
    {
        public virtual int Id { get; set; }
        public virtual string Address { get; set; }
        public virtual List<LibraryBookAndNumber> Books { get; set; }

    }
}