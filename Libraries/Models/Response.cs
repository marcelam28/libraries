﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Libraries.Models
{
    public class Response
    {
        public bool Success { get; set; }
        public Object Data { get; set; }
    }
}