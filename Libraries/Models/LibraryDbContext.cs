﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Libraries.Models
{
    public class LibraryDbContext : DbContext
    {
        public DbSet<Library> Libraries { get; set; }

        public System.Data.Entity.DbSet<Libraries.Models.Book> Books { get; set; }

        public System.Data.Entity.DbSet<Libraries.Models.LibraryBookAndNumber> LibraryBookAndNumbers { get; set; }

    }
}